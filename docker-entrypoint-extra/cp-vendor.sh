#!/bin/bash
mkdir wp-content/plugins/
mkdir wp-content/themes/

cp -TRf vendor/plugins/ wp-content/plugins/
cp -TRf vendor/themes/ wp-content/themes/

cp -TRf vendor/foradoeixo/BotaPressao/ wp-content/plugins/BotaPressao/
cp -TRf vendor/foradoeixo/342/ wp-content/themes/342/


for dir in plugins/*/; do ln -s ../../$dir wp-content/plugins/$(basename $dir) ; done

for dir in themes/*/; do ln -s ../../$dir wp-content/themes/$(basename $dir) ; done
